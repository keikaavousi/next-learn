import React from 'react'
import Link from 'next/link'

const About = () => {
    return (
        <div>
            <Link href="/">
                <a title="main page">Go to main page</a>
            </Link>
            
            <h2>About</h2>
        </div>
    )
}

export default About;
