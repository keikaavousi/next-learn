import React from 'react'

const Single = (props) => {
    return (
        <div>
            <h2>{props.result.title}</h2>
            <h3>{props.result.price}</h3>
        </div>
    )
}

export default Single;

Single.getInitialProps = async ctx => {
    const url = encodeURI(`https://fakestoreapi.com/products/${ctx.query.id}`)
    const fetched = await fetch(url)
    const result = await fetched.json()

    return{result}
}
