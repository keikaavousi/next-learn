import React from 'react'
import Link from 'next/link'

const Home = () => {
  return (
    <div>
            <Link href="/about">
                <a title="about page">Go to about page</a>
            </Link>
        <h2>Index</h2>
    </div>
  )
}

export default Home;

