// import React,{useState,useEffect} from 'react'



// const Products = () => {
//     const [products, setProducts] = useState([])
//     useEffect(() => {
//         fetch('https://fakestoreapi.com/products')
//         .then(res=>res.json())
//         .then(rs=>setProducts(rs))
//     }, []);

//     return (
//         <div>
//             {products.map(n=>{
//                 return(
//                     <h2 key={n.id}>{n.title}</h2>
//                 )
//             })}
//         </div>
//     )
// }

// export default Products;




import React from 'react'
import fetch from 'isomorphic-fetch'
import Link from 'next/link'

const Products = (props) => {
    return (
        <div>
             {props.result.map(n=>{
                 return(
                    <Link href="/products/[id]" as={`/products/${n.id}`}>
                        <a>
                            <h2 key={n.id}>
                                {n.title}
                            </h2>
                        </a>
                    </Link>
                )
            })}
        </div>
    )
}

export default Products;


Products.getInitialProps = async ctx => {
    const url = encodeURI('https://fakestoreapi.com/products')
    const fetched = await fetch(url)
    const result = await fetched.json()

    return{result}
}




